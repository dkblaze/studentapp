//
//  Student.swift
//  StudentApp
//
//  
//

import Foundation
import FirebaseFirestoreSwift


struct Student:Identifiable,Codable{
    
    @DocumentID var id:String?
    var firstname:String
    var lastname:String
    var age:Int
    var departament:String
    var clubs:Club
}
struct Club:Codable{
    var aerospace:Bool
    var medicine:Bool
    var design:Bool
    var physics:Bool
}

