//
//  StudentViewModel.swift
//  StudentApp
//
//  
//

import Combine
import FirebaseFirestore
import Foundation

class StudentViewModel: ObservableObject {
    @Published var student: Student
    @Published var modified = false
    private var cancellables = Set<AnyCancellable>()
    private var db = Firestore.firestore()

    init(student: Student = Student(firstname: "", lastname: "", age: 0, departament: "", clubs: Club(aerospace: false, medicine: false, design: false, physics: false))) {
        self.student = student

        self.$student
            .dropFirst()
            .sink { [weak self] _ in
                self?.modified = true
            }
            
            .store(in: &cancellables)
        
        
    }

    private func addStudent(student: Student) {
        do {
            _ = try db.collection("students").addDocument(from: student)

        } catch {
            print(error)
        }
    }

    private func updatedata(student: Student) {
        if let studentId = student.id {
            do {
                try db.collection("students").document(studentId).setData(from: student)
            } catch {
                print("Updating Error - \(error.localizedDescription)")
            }
        }
    }

    private func removeStudent() {
        if let studentID = student.id {
            db.collection("students").document(studentID).delete { error in
                if let error = error {
                    print("Remove error -\(error.localizedDescription)")
                }
            }
        }
    }

    private func updateOrAdd() {
        if let _ = student.id {
            self.updatedata(student: self.student)
        } else {
            addStudent(student: self.student)
        }
    }

    func handleSaveTapped() {
        self.updateOrAdd()
    }

    func handleDeleteTapped() {
        self.removeStudent()
    }
}
