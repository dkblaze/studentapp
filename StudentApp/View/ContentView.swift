//
//  ContentView.swift
//  StudentApp
//
//  
//

import Combine
import SwiftUI

struct ContentView: View {
    @ObservedObject var viewModel = StudentsViewModel()
    @State var addStudentSheet: Bool = false
    @State var searchStudent: String = ""
    
    private var addStudentButton: some View {
        Button(action: { self.addStudentSheet.toggle() }) {
            Image(systemName: "plus.circle.fill")
                .font(.system(size: 30))
                .foregroundColor(.red)
        }
    }

    private func studentsList(student: Student) -> some View {
        NavigationLink(destination: StudentInformationView(student: student)) {
            VStack(alignment: .center) {
                Text("\(student.firstname) \(student.lastname)")
                    .font(.headline)
                Text(student.departament)
                    .font(.subheadline)
            }
            .padding()
        }
    }

    var body: some View {
        NavigationView {
            VStack {
                SearchBar(text: $searchStudent)
                
                List {
                    if searchStudent.isEmpty {
                        Section(header: Text("Physics")) {
                            ForEach(viewModel.students) { studentResult in
                            
                                if studentResult.clubs.physics {
                                    studentsList(student: studentResult)
                                }
                            }
                        }
                        Section(header: Text("Aerospace")) {
                            ForEach(viewModel.students) { studentResult in
                            
                                if studentResult.clubs.aerospace {
                                    studentsList(student: studentResult)
                                }
                            }
                        }
                        Section(header: Text("Aerospace")) {
                            ForEach(viewModel.students) { studentResult in
                            
                                if studentResult.clubs.aerospace {
                                    studentsList(student: studentResult)
                                }
                            }
                        }
                        Section(header: Text("Medicine")) {
                            ForEach(viewModel.students) { studentResult in
                            
                                if studentResult.clubs.medicine {
                                    studentsList(student: studentResult)
                                }
                            }
                        }
                    } else {
                        ForEach(viewModel.students) { studentResult in
                            if studentResult.firstname == searchStudent || studentResult.lastname == searchStudent {
                                studentsList(student: studentResult)
                            }
                        }
                    }
                }
                .navigationBarTitle("Wellcome back")
                    
                .onAppear {
                    self.viewModel.getStudents()
                }
                .onDisappear {
                    self.viewModel.unsubscribe()
                }
                .navigationBarItems(trailing: addStudentButton)
                .sheet(isPresented: $addStudentSheet, content: {
                    AddStudentView()
                })
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
