//
//  StudentsViewModel.swift
//  StudentApp
//
//  
//

import Combine
import FirebaseFirestore
import Foundation

class StudentsViewModel: ObservableObject {
    @Published var students = [Student]()
    
    private var db = Firestore.firestore()
    private var listenerRegistration: ListenerRegistration?
    
    deinit {
        unsubscribe()
    }
    
    func unsubscribe() {
        if listenerRegistration != nil {
            listenerRegistration?.remove()
            listenerRegistration = nil
        }
    }

    func getStudents() {
        if listenerRegistration == nil {
            listenerRegistration = db.collection("students").addSnapshotListener { querySnap, _ in
                guard let documents = querySnap?.documents else {
                    print("No documents")
                    return
                }
            
                self.students = documents.compactMap { queryDocumentSnap in
                    try? queryDocumentSnap.data(as: Student.self)
                }
            }
        }
    }

    func removeStudents(at index: IndexSet) {
        let student = index.lazy.map { self.students[$0] }
        student.forEach {
            student in
            
            if let documentId = student.id {
                db.collection("students").document(documentId).delete {
                    error in
                    if let error = error {
                        print("Unable to remove document \(error.localizedDescription)")
                    }
                }
            }
        }
    }
}
