//
//  StudentAppApp.swift
//  StudentApp
//
//  
//

import SwiftUI
import Firebase
@main
struct StudentAppApp: App {
    init(){
        FirebaseApp.configure()
    }
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
