//
//  AddStudentView.swift
//  StudentApp
//
//  
//

import SwiftUI

struct AddStudentView: View {
    @ObservedObject var studentViewModel = StudentViewModel()
    @Environment(\.presentationMode) private var presentationMode
    var mode: Mode = .create
    var completionHandler: ((Result<Action, Error>) -> Void)?

    var saveButton: some View {
        Button(action: {
            self.handleSaveTapped()

        }) {
            Text(mode == .create ? "Save" : "Done")
                .font(.system(.headline, design: .rounded))
                .foregroundColor(Color.blue)
                .lineLimit(3)
                .padding()
        }
        .disabled(!studentViewModel.modified)
    }

    var cancelButton: some View {
        Button(action: { self.handleCancelTapped() }) {
            Image(systemName: "xmark.circle.fill")
                .font(.system(size: 30, weight: .heavy))
                .foregroundColor(Color.red)
                .padding(.top, 2)
        }
    }

    var deleteButton: some View {
        Button(action: {
            self.handleDeleteTappeed()
        }) {
            Text("Delete")
                .font(.system(.headline, design: .rounded))
                .foregroundColor(mode == .create ? Color.blue : Color.red)
                .lineLimit(3)
                .padding()
        }
    }

    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Identity")) {
                    TextField("Firstname", text: $studentViewModel.student.firstname)
                        .font(.system(.headline, design: .rounded))
                        .foregroundColor(.primary)
                        .lineLimit(3)
                        .padding()

                    TextField("Lastname", text: $studentViewModel.student.lastname)
                        .font(.system(.headline, design: .rounded))
                        .foregroundColor(.primary)
                        .lineLimit(3)
                        .padding()
                    TextField("Age", value: $studentViewModel.student.age, formatter: NumberFormatter())
                        .font(.system(.headline, design: .rounded))
                        .foregroundColor(.primary)
                        .lineLimit(3)
                        .padding()
                }
                Section(header: Text("Departament")) {
                    TextField("Field of study", text: $studentViewModel.student.departament)
                        .font(.system(.headline, design: .rounded))
                        .foregroundColor(.primary)
                        .lineLimit(3)
                        .padding()
                }
                Section(header: Text("Clubs")) {
                    Toggle(isOn: $studentViewModel.student.clubs.physics) {
                        Text("Physics")
                            .font(.system(.headline, design: .rounded))
                            .foregroundColor(.primary)
                            .lineLimit(3)
                            .padding()
                    }
                    Toggle(isOn: $studentViewModel.student.clubs.medicine) {
                        Text("Medicine")
                            .font(.system(.headline, design: .rounded))
                            .foregroundColor(.primary)
                            .lineLimit(3)
                            .padding()
                    }
                    Toggle(isOn: $studentViewModel.student.clubs.design) {
                        Text("Design")
                            .font(.system(.headline, design: .rounded))
                            .foregroundColor(.primary)
                            .lineLimit(3)
                            .padding()
                    }
                    Toggle(isOn: $studentViewModel.student.clubs.aerospace) {
                        Text("Aerospace")
                            .font(.system(.headline, design: .rounded))
                            .foregroundColor(.primary)
                            .lineLimit(3)
                            .padding()
                    }
                }
                Section {
                    self.saveButton
                    if mode == .edit {
                        self.deleteButton
                    }
                }
            }

            .navigationTitle("Student Information")
            .navigationBarItems(
                trailing: cancelButton
            )
        }
    }

    func handleCancelTapped() {
        self.presentationMode.wrappedValue.dismiss()
    }

    func handleSaveTapped() {
        self.studentViewModel.handleSaveTapped()
        self.presentationMode.wrappedValue.dismiss()
    }

    func handleDeleteTappeed() {
        self.studentViewModel.handleDeleteTapped()
        self.presentationMode.wrappedValue.dismiss()
        self.completionHandler?(.success(.delete))
    }
}

enum Mode {
    case create
    case edit
}

enum Action {
    case delete
    case save
    case cancel
}
