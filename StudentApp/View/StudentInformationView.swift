//
//  StudentInformationView.swift
//  StudentApp
//
//  
//

import SwiftUI

struct StudentInformationView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var editStudentSheet = false

    var student: Student
    var body: some View {
        VStack {
            Form {
                Section(header: Text("Identity")) {
                    Text(student.firstname)
                        .font(.system(.headline, design: .rounded))
                        .foregroundColor(.primary)
                        .lineLimit(3)
                        .padding()

                    Text(student.lastname)
                        .font(.system(.headline, design: .rounded))
                        .foregroundColor(.primary)
                        .lineLimit(3)
                        .padding()
                    Text("\(student.age) years")
                        .font(.system(.headline, design: .rounded))
                        .foregroundColor(.primary)
                        .lineLimit(3)
                        .padding()
                }
                Section(header: Text("Departament")) {
                    Text(student.departament)
                        .font(.system(.headline, design: .rounded))
                        .foregroundColor(.primary)
                        .lineLimit(3)
                        .padding()
                }
                Section(header: Text("Clubs")) {
                    if student.clubs.physics {
                        Text("Physics")
                            .font(.system(.headline, design: .rounded))
                            .foregroundColor(.primary)
                            .lineLimit(3)
                            .padding()
                    }

                    if student.clubs.medicine {
                        Text("Medicine")
                            .font(.system(.headline, design: .rounded))
                            .foregroundColor(.primary)
                            .lineLimit(3)
                            .padding()
                    }
                    if student.clubs.design {
                        Text("Design")
                            .font(.system(.headline, design: .rounded))
                            .foregroundColor(.primary)
                            .lineLimit(3)
                            .padding()
                    }
                    if student.clubs.aerospace {
                        Text("Aerospace")
                            .font(.system(.headline, design: .rounded))
                            .foregroundColor(.primary)
                            .lineLimit(3)
                            .padding()
                    }
                }
            }.navigationTitle("Student Inforation")
                .navigationBarItems(trailing:
                    Button(action: { self.editStudentSheet.toggle() }, label: {
                        Text("Edit")
                    })
                )
        }.sheet(isPresented: $editStudentSheet) {
            AddStudentView(studentViewModel: StudentViewModel(student: student), mode: .edit) {
                result in
                if case .success(let action) = result, action == .delete {
                    self.presentationMode.wrappedValue.dismiss()
                }
            }
        }
    }
}
